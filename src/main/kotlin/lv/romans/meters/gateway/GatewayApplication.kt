package lv.romans.meters.gateway

import com.auth0.jwt.JWT
import lv.romans.meters.commons.Constants
import lv.romans.meters.commons.Constants.AUTH_PREFIX
import lv.romans.meters.commons.Constants.CLAIM_ROLE
import lv.romans.meters.commons.Constants.HEADER_USER_ID
import lv.romans.meters.commons.Constants.HEADER_USER_ROLES
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.client.ServiceInstance
import org.springframework.cloud.client.discovery.EnableDiscoveryClient
import org.springframework.cloud.client.loadbalancer.reactive.ReactiveLoadBalancer
import org.springframework.cloud.client.loadbalancer.reactive.ReactorLoadBalancerExchangeFilterFunction
import org.springframework.cloud.gateway.filter.GatewayFilterChain
import org.springframework.cloud.gateway.filter.GlobalFilter
import org.springframework.cloud.gateway.route.RouteLocator
import org.springframework.cloud.gateway.route.builder.PredicateSpec
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder
import org.springframework.context.annotation.Bean
import org.springframework.core.Ordered
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.server.ServerWebExchange
import reactor.core.publisher.Mono
import java.util.logging.Level
import java.util.logging.Logger


@EnableDiscoveryClient
@SpringBootApplication
class GatewayApplication {
    @Bean
    fun customRouteLocator(builder: RouteLocatorBuilder): RouteLocator? {
        return builder
                .routes()
                .route("auth") { r: PredicateSpec ->
                    r.path("/api/login").or()
                            .path("/api/changePassword")
                            .filters { it.rewritePath("^/api", "") }
                            .uri("lb://AUTH-SERVICE")

                }.route("admin") { r: PredicateSpec ->
                    r.path("/api/admin/**")
                            .filters { it.rewritePath("^/api/admin", "") }
                            .uri("lb://ADMIN-SERVICE")
                }
                .route("managers") { r: PredicateSpec ->
                    r.path("/api/managers/**")
                            .filters { it.rewritePath("^/api", "") }
                            .uri("lb://MANAGER-SERVICE")
                }.route("testing") { r: PredicateSpec ->
                    r.path("/api/test/revertPasswordChange")
                            .filters { it.rewritePath("^/api", "") }
                            .uri("lb://AUTH-SERVICE")
                }
                .build()
    }

    @Bean
    fun webClient(loadBalancerClient: ReactiveLoadBalancer.Factory<ServiceInstance>): WebClient {
        return WebClient.builder()
                .filter(ReactorLoadBalancerExchangeFilterFunction(loadBalancerClient))
                .build()
    }
}

fun main(args: Array<String>) {
    runApplication<GatewayApplication>(*args)
}

@Component
class RequestFilter(val webClient: WebClient) : GlobalFilter, Ordered {
    @ResponseStatus(HttpStatus.FORBIDDEN)
    object InvalidToken : Exception()

    override fun filter(exchange: ServerWebExchange, chain: GatewayFilterChain): Mono<Void> {
        val originalRequest = exchange.request
        val authorization = originalRequest.headers[Constants.HEADER_AUTHORIZATION]
        val originalPath = originalRequest.path.value()
        val isLoginRequest = originalPath.startsWith("/api/login")
        val isChangePasswordRequest = originalPath.startsWith("/api/changePassword")
        return when {
            isLoginRequest -> chain.filter(exchange)
            authorization.isNullOrEmpty() -> Mono.error(InvalidToken)
            needToChangePassword(authorization[0]) -> if (isChangePasswordRequest) chain.filter(exchange) else Mono.error(InvalidToken)
            else -> webClient.get().uri("lb://AUTH-SERVICE/validate")
                    .header(Constants.HEADER_AUTHORIZATION, authorization[0]).exchange()
                    .flatMap {
                        if (it.statusCode().is2xxSuccessful) {
                            val token = JWT.decode(authorization[0].removePrefix(AUTH_PREFIX))
                            val newRequest = originalRequest.mutate()
                                    .header(HEADER_USER_ID, token.subject)
                                    .header(HEADER_USER_ROLES, token.claims[CLAIM_ROLE]!!.asString())
                                    .build()
                            chain.filter(exchange.mutate().request(newRequest).build())
                        } else Mono.error(InvalidToken)
                    }

        }
    }

    private fun needToChangePassword(authorization: String) =
            JWT.decode(authorization.removePrefix(Constants.AUTH_PREFIX)).getClaim(Constants.PASSWORD_CHANGE_REQUIRED).asBoolean()

    override fun getOrder() = Ordered.HIGHEST_PRECEDENCE
}

